# UPSDIAGD

Scripts are supplied that allow you to 
* interrogate a `usbhid-ups` compliant UPS
* store specific data in a MySQL database (server not included; you'll need to set-up your own)
* query the (not included) MySQL database for interesting data
* plot some graphs
* push graphs to a website (webserver not included; you'll need to set-up your own)

## Installing

```
sudo su -
cd /path/to/where/you/want/store/upsdiagd
git clone https://gitlab.com/mausy5043-diagnostics/upsdiagd.git
cd upsdiagd
./install.sh
./update.sh
```

## Additional software:
This repo assumes you have already installed and configured `nut`.  
For an example of that, please refer to [these files](https://gitlab.com/mausy5043-installer/raspboot/tree/master/rbups)

## Hardware:
Raspberry Pi 1B or better, connected to a UPS via USB cable.

Known to work with:
- APC Back-UPS 700 (BE-700GR)
- EATON ProtectionStation 650

(probably all UPSes that support the `usbhid-ups` driver)
